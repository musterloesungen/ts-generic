interface IVehicle {
    getVehicleType(): string;
    getMaxSpeed(): number;
}

class Car implements IVehicle {
    getVehicleType(): string {
        return "Car";
    }

    getMaxSpeed(): number {
        return 200;
    }
}

class Bike implements IVehicle {
    getVehicleType(): string {
        return "Bike";
    }

    getMaxSpeed(): number {
        return 50;
    }
}

class VehicleFactory {
    static createVehicle<T extends IVehicle>(type: { new (): T }): T {
        return new type();
    }
}

// Beispiel zur Verwendung
const car = VehicleFactory.createVehicle(Car);
console.log(car.getVehicleType());  // Ausgabe: Car

const bike = VehicleFactory.createVehicle(Bike);
console.log(bike.getVehicleType());  // Ausgabe: Bike
